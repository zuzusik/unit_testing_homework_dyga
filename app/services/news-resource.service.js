angular.module('truthNews')
  .service('newsResourceSrv', ['$resource', '$http', function ($resource, $http) {
    var newTransformResponse = $http.defaults.transformResponse.concat(addHeaders);

    function addHeaders (data, headers) {
      return {data: data, headers: headers};
    }

    return $resource('/api/news/:newsItemId', {}, {
      get: {
        method: 'GET',
        transformResponse: newTransformResponse
      },
      query: {
        method: 'GET',
        transformResponse: newTransformResponse
      }
    });
  }]);
