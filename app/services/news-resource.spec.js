describe('news-resource', () => {
  'use strict';

  beforeEach(module('truthNews'));

  describe('service', () => {
    let url = '/api/news',
      newsResourceSrv,
      $httpBackend,
      mocks;

    beforeEach(() => {
      mocks = {
        response: {
          id: 183439590,
          text: 'Beatae adipisci omnis consequuntur temporibus.'
        },
        headers: {
          firstToken: 'first-token-value',
          secondToken: 'second-token-value'
        }
      };

      inject((_$httpBackend_, _newsResourceSrv_) => {
        $httpBackend = _$httpBackend_;
        newsResourceSrv = _newsResourceSrv_;
      });

      $httpBackend.whenGET(url).respond(200, mocks.response, mocks.headers);
    });

    afterEach(() => {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('transforms response data', () => {
      let res = newsResourceSrv.get();
      $httpBackend.flush();

      expect(res.data).toEqual(mocks.response);
    });

    it('transforms response headers', () => {
      let res = newsResourceSrv.get();
      $httpBackend.flush();

      Object.keys(mocks.headers).forEach((key) => {
        expect(res.headers(key)).toEqual(mocks.headers[key]);
      });
    });
  });
});
