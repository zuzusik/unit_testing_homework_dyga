angular.module('truthNews').config(['$httpProvider', 'errorNotifierSrvProvider',
    function ($httpProvider, errorNotifierSrvProvider) {
  $httpProvider.interceptors.push('errorNotifierInterceptor');
  errorNotifierSrvProvider.setErrorShowTime(5000);
}]);
