/**
 *  * Created by konstantind on 15  Mar 2016.
 */
describe('newsItemCtrl', () => {
  'use strict';

  let $scope,
    newsResourceDefer,
    mocks,
    newsItem;

  beforeEach(module('truthNews'));

  beforeEach(() => {
    newsItem = {
      id: 649813286,
      title: 'Dolores laborum aut placeat.',
      text: 'Doloremque soluta aut error delectus culpa et.\nEst reprehenderit itaque.\nLabore adipisci fugiat sint omnis laborum quis ullam culpa omnis.\nQui expedita corporis voluptatem eveniet dignissimos inventore unde.\nSunt esse dolorem voluptas iusto.\n \rIn dolorem cum est aliquam non consectetur et.\nVelit rerum et assumenda.\nSint error in tempora ipsum.\nAd placeat nobis omnis quia dicta ducimus ipsum assumenda.\nModi veniam illo suscipit et velit quia qui.\nEum laudantium sed et illo.\n \rDolor incidunt repudiandae.\nEst cupiditate nihil nihil vitae alias quas.\nDolorem corporis totam aut quidem hic.\nCupiditate labore suscipit nihil ipsam qui ipsa modi voluptas dolorem.\nUt est aperiam enim aut a.\n \rMolestiae incidunt aut id maiores non voluptatem recusandae et ipsa.\nPerspiciatis veniam dolor quaerat rerum nesciunt minima qui quia dolores.\nOccaecati libero aspernatur non.\n \rOccaecati harum explicabo sed qui asperiores tempore et sequi.\nIusto est non officiis consequatur consequuntur.\nRerum esse et ex non ab iste.\nSit perspiciatis impedit.\nMolestiae voluptatem voluptate provident quae et et fuga.\nCupiditate quia magni quaerat accusamus velit ullam.',
      date: '2016-01-03T05:14:15.465Z',
      picture: 'http://lorempixel.com/320/240/?rand=0.4943678164854646'
    };

    mocks = {
      $routeParams: {
        newsItemId: 100500
      },
      newsResourceSrv: {
        get: jasmine.createSpy('newsResourceSrv.get').and.callFake(() => {
          return { $promise: newsResourceDefer.promise };
        })
      },
      loaderSrv: {
        getLoadingState: angular.noop,
        show: jasmine.createSpy('loaderSrv.show'),
        hide: jasmine.createSpy('loaderSrv.hide')
      }
    };

    module(($provide) => {
      $provide.value('$routeParams', mocks.$routeParams);
      $provide.value('newsResourceSrv', mocks.newsResourceSrv);
      $provide.value('loaderSrv', mocks.loaderSrv);
    });

    inject(($rootScope, $controller, $q) => {
      $scope = $rootScope.$new();

      newsResourceDefer = $q.defer();

      $controller('newsItemCtrl', { $scope });
    });
  });

  it('shows loader from the start', () => {
    expect(mocks.loaderSrv.show).toHaveBeenCalled();
  });

  it('loads peace of news depending on $routeParams', () => {
    expect(mocks.newsResourceSrv.get).toHaveBeenCalledWith({ newsItemId: mocks.$routeParams.newsItemId });
  });

  it('splits news text into paragraphs', () => {
    newsResourceDefer.resolve({ data: newsItem });

    $scope.$digest();

    expect(newsItem.paragraphs).toEqual([
      'Doloremque soluta aut error delectus culpa et.\nEst reprehenderit itaque.\nLabore adipisci fugiat sint omnis laborum quis ullam culpa omnis.\nQui expedita corporis voluptatem eveniet dignissimos inventore unde.\nSunt esse dolorem voluptas iusto.',
      'In dolorem cum est aliquam non consectetur et.\nVelit rerum et assumenda.\nSint error in tempora ipsum.\nAd placeat nobis omnis quia dicta ducimus ipsum assumenda.\nModi veniam illo suscipit et velit quia qui.\nEum laudantium sed et illo.',
      'Dolor incidunt repudiandae.\nEst cupiditate nihil nihil vitae alias quas.\nDolorem corporis totam aut quidem hic.\nCupiditate labore suscipit nihil ipsam qui ipsa modi voluptas dolorem.\nUt est aperiam enim aut a.',
      'Molestiae incidunt aut id maiores non voluptatem recusandae et ipsa.\nPerspiciatis veniam dolor quaerat rerum nesciunt minima qui quia dolores.\nOccaecati libero aspernatur non.',
      'Occaecati harum explicabo sed qui asperiores tempore et sequi.\nIusto est non officiis consequatur consequuntur.\nRerum esse et ex non ab iste.\nSit perspiciatis impedit.\nMolestiae voluptatem voluptate provident quae et et fuga.\nCupiditate quia magni quaerat accusamus velit ullam.'
    ]);
  });

  it('bind news item to scope', () => {
    newsResourceDefer.resolve({ data: newsItem });

    $scope.$digest();

    expect($scope.newsItem).toEqual(newsItem);
  });

  it('hides loader on response', () => {
    newsResourceDefer.resolve({ data: newsItem });

    $scope.$digest();

    expect(mocks.loaderSrv.hide).toHaveBeenCalled();
  });

  it('hides loader on response error', () => {
    newsResourceDefer.reject();

    $scope.$digest();

    expect(mocks.loaderSrv.hide).toHaveBeenCalled();
  });
});


