/**
 * Created by konstantind on 15  Mar 2016.
 */
describe('newsListCtrl', () => {
  'use strict';

  const NEWS_OFFSET = 0;
  const NEWS_PAGE_SIZE = 10;

  let newsResourceDefer,
    $scope,
    mocks;

  beforeEach(module('truthNews'));

  beforeEach(() => {
    mocks = {
      newsResourceSrv: {
        query: jasmine.createSpy('newsResourceSrv.query').and.callFake(() => {
          return { $promise: newsResourceDefer.promise };
        })
      },
      loaderSrv: {
        getLoadingState: angular.noop,
        show: jasmine.createSpy('loaderSrv.show'),
        hide: jasmine.createSpy('loaderSrv.hide')
      }
    };

    module(($provide) => {
      $provide.value('newsResourceSrv', mocks.newsResourceSrv);
      $provide.value('loaderSrv', mocks.loaderSrv);
    });

    inject(($rootScope, $controller, $q) => {
      $scope = $rootScope.$new();

      newsResourceDefer = $q.defer();

      $controller('newsListCtrl', { $scope });
    });
  });

  it('has empty news list from the start', () => {
    expect($scope.newsList).toEqual([]);
  });

  it('binds loading state getter to scope', () => {
    expect($scope.isNewsLoading).toBe(mocks.loaderSrv.getLoadingState);
  });

  it('loads first news page', () => {
    expect(mocks.loaderSrv.show).toHaveBeenCalled();
    expect(mocks.newsResourceSrv.query).toHaveBeenCalledWith({
      offset: NEWS_OFFSET,
      size: NEWS_PAGE_SIZE
    });
  });

  describe('loadNextNewsPage()', () => {
    let news = [{}, {}, {}];

    it('shows loader on request', () => {
      $scope.loadNextNewsPage();

      expect(mocks.loaderSrv.show).toHaveBeenCalled();
    });

    it('adds loaded news to list', () => {
      let prevListLength = $scope.newsList.length,
        newListLength;

      newsResourceDefer.resolve({ data: news });

      $scope.$digest();

      newListLength = $scope.newsList.length;

      expect(newListLength).toBe(prevListLength + news.length);

      news.forEach((newsItem) => {
        expect($scope.newsList).toContain(newsItem);
      });
    });

    it('get next page on second request', () => {
      newsResourceDefer.resolve({ data: news });

      $scope.$digest();

      $scope.loadNextNewsPage();

      expect(mocks.newsResourceSrv.query).toHaveBeenCalledWith({
        offset: NEWS_OFFSET + NEWS_PAGE_SIZE,
        size: NEWS_PAGE_SIZE
      });
    });

    it('hides loader on response', () => {
      newsResourceDefer.resolve({ data: news });

      $scope.$digest();

      expect(mocks.loaderSrv.hide).toHaveBeenCalled();
    });

    it('hides loader on response error', () => {
      newsResourceDefer.reject();

      $scope.$digest();

      expect(mocks.loaderSrv.hide).toHaveBeenCalled();
    });
  });
});
