describe('errorNotifierInterceptor', () => {
  'use strict';

  let errorNotifierInterceptor,
    mocks;

  beforeEach(() => {
    mocks = {
      errorNotifierSrv: {
        addError: jasmine.createSpy('errorNotifierSrv.addError')
      }
    };

    module('truthNews', ($provide) => {
      $provide.value('errorNotifierSrv', mocks.errorNotifierSrv);
    });

    inject((_errorNotifierInterceptor_) => {
      errorNotifierInterceptor = _errorNotifierInterceptor_;
    });
  });

  it('has response error handler', () => {
    expect(angular.isFunction(errorNotifierInterceptor.responseError)).toBeTruthy();
  });

  it('adds error on http response error', () => {
    let rejection = {};

    errorNotifierInterceptor.responseError(rejection);

    expect(mocks.errorNotifierSrv.addError).toHaveBeenCalled();
  });

  describe('error message', () => {
    it('sets to default when status and statusText is empty', () => {
      let rejection = {};

      errorNotifierInterceptor.responseError(rejection);

      expect(mocks.errorNotifierSrv.addError).toHaveBeenCalledWith({ message: ' Data loading failed.' });
    });

    it('contains error status if passed', () => {
      let rejection = {
        status: 500
      };

      errorNotifierInterceptor.responseError(rejection);

      expect(mocks.errorNotifierSrv.addError.calls.argsFor(0)[0].message).toContain('500');
    });

    it('contains error statusText if passed', () => {
      let rejection = {
        statusText: 'Lorem ipsum'
      };

      errorNotifierInterceptor.responseError(rejection);

      expect(mocks.errorNotifierSrv.addError.calls.argsFor(0)[0].message).toContain('Lorem ipsum');
    });

    it('contains error status and statusText if both passed', () => {
      let rejection = {
        status: 500,
        statusText: 'Lorem ipsum'
      };

      errorNotifierInterceptor.responseError(rejection);

      expect(mocks.errorNotifierSrv.addError.calls.argsFor(0)[0].message).toContain('500');
      expect(mocks.errorNotifierSrv.addError.calls.argsFor(0)[0].message).toContain('Lorem ipsum');
    });
  });
});
