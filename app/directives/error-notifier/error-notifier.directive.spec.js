describe('errorNotifier directive', () => {
  'use strict';

  const REMOVE_ERROR_BUTTON_CLASS = 'remove-error';

  let mocks,
    errors,
    $scope,
    directiveElt;

  beforeEach(() => {
    errors = [{
      message: Math.random().toString()
    }, {
      message: Math.random().toString()
    }, {
      message: Math.random().toString()
    }];

    mocks = {
      errorNotifierSrv: {
        getErrors: jasmine.createSpy('errorNotifierSrv.getErrors').and.callFake(() => {
          return errors;
        }),
        removeError: jasmine.createSpy('errorNotifierSrv.removeError')
      }
    };

    module('truthNews', ($provide) => {
      $provide.value('errorNotifierSrv', mocks.errorNotifierSrv);
    });

    inject(($rootScope, $compile) => {
      directiveElt = $compile('<div error-notifier></div>')($rootScope.$new());

      $rootScope.$digest();

      $scope = directiveElt.isolateScope();
    });
  });

  it('assigns error service on scope', () => {
    expect($scope.errorService).toBe(mocks.errorNotifierSrv);
  });

  it('displays errors from service', () => {
    errors.forEach((error) => {
      expect(directiveElt.text()).toContain(error.message);
    });
  });

  it('has close button for every error', () => {
    let removeButtons = directiveElt[0].querySelectorAll(`.${ REMOVE_ERROR_BUTTON_CLASS }`);

    Array.prototype.forEach.call(removeButtons, (button, index) => {
      button.click();
      expect(mocks.errorNotifierSrv.removeError).toHaveBeenCalledWith(errors[index]);
    });
  });
});
