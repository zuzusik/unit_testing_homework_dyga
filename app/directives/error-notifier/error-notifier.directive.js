angular.module('truthNews').directive('errorNotifier', function () {
  return {
    scope: {},
    templateUrl: 'directives/error-notifier/error-notifier.template.html',
    controller: ['$scope', 'errorNotifierSrv', function($scope, errorNotifierSrv) {
      $scope.errorService = errorNotifierSrv;
    }]
  }
});
