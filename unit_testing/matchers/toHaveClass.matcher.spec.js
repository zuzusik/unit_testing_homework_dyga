describe('toHaveClass matcher', () => {
  'use strict';

  let elem;

  beforeEach(() => {
    elem = document.createElement('div');

    jasmine.addMatchers(customMatchers);
  });

  it('throw error if not HTMLNode is checking', () => {
    expect(() => {
      expect().toHaveClass('class-name');
    }).toThrowError();

    expect(() => {
      expect(1).toHaveClass('class-name');
    }).toThrowError();

    expect(() => {
      expect('1').toHaveClass('class-name');
    }).toThrowError();

    expect(() => {
      expect([]).toHaveClass('class-name');
    }).toThrowError();

    expect(() => {
      expect({}).toHaveClass('class-name');
    }).toThrowError();
  });

  it('throw error if expected value is not a string', () => {
    expect(() => {
      expect(elem).toHaveClass();
    }).toThrowError();

    expect(() => {
      expect(elem).toHaveClass(1);
    }).toThrowError();

    expect(() => {
      expect(elem).toHaveClass([]);
    }).toThrowError();

    expect(() => {
      expect(elem).toHaveClass({});
    }).toThrowError();

    expect(() => {
      expect(elem).toHaveClass(elem);
    }).toThrowError();
  });

  it('do not pass if elements don`t have expected class', () => {
    expect(elem).not.toHaveClass('trololo');
  });

  it('pass if elements have expected class', () => {
    elem.className = 'trololo';
    expect(elem).toHaveClass('trololo');
  });
});
